<?php 

include ("../../../connection.php");
$edit_id = $_POST["id"]; 
$select = "SELECT username FROM tbl_user WHERE id = '$edit_id'";
$result = $connection->query($select);
$data = $result->fetch_assoc();

?>

<div>
    <p>
        <label for="txt-user-account-name-edit">User Name</label>&nbsp;<span style="color: red;">*</span><br />
        <input id="txt-user-account-name-edit" class="input new_theme_textbox ui-corner-all focus" type="text" value="<?= $data["username"] ?>" />
    </p>
    <p>
        <label for="txt-user-account-old-password-edit">Old Password</label>&nbsp;<span style="color: red;">*</span><br />
        <input id="txt-user-account-old-password-edit" class="input new_theme_textbox ui-corner-all focus" type="password" value="" />
    </p>
    <p>
        <label for="txt-user-account-new-password-edit">New Password</label>&nbsp;<span style="color: red;"></span><br />
        <input id="txt-user-account-new-password-edit" class="input new_theme_textbox ui-corner-all focus" type="password" />
    </p>
    <p>
        <label for="txt-user-account-confirm-edit">Confirm New</label>&nbsp;<span style="color: red;"></span><br />
        <input id="txt-user-account-confirm-edit" class="input new_theme_textbox ui-corner-all focus" type="password" />
    </p>
	<br />
	<span>Fields has asterisk(<span style="color: red;">*</span>) mark are required</span>
</div>

<script type="text/javascript">
$("#txt-user-account-name-edit").select().keydown(function(e) {
    if (e.keyCode == "13") {
        $(":button:contains(Save)").trigger("click");
    }
});

function save_user_account() {
    if ($("#txt-user-account-name-edit").val() == "") {
        $("#txt-user-account-name-edit").addClass("ui-state-error");
        $("#msg-status-1").show()
        .css({
            color: "red",
            padding: "4px 8px"
        })
        .html("Data required!");
    } else {
        $("#txt-user-account-name-edit").removeClass("ui-state-error");
        $("#msg-status-1").hide();
    }
    if ($("#txt-user-account-name-edit").hasClass("ui-state-error")) {
        $("#txt-user-account-name-edit").focus(); return;
    }
	
    $.ajax({
        url: "content/manage-user/account/save.php",
        data: {
            id: <?= $edit_id ?>,
            username: $("#txt-user-account-name-edit").val(),
            password: $("#txt-user-account-new-password-edit").val(),
			old_password: $("#txt-user-account-old-password-edit").val()
        },
        type: "post",
        dataType: "html",
        success: function(result) {
            $("#tbl-user-account tbody").load("content/manage-user/account/reload.php");
            $("#dlg-user-account-edit").dialog("close");
        },
        error: function(xhr, status, ex) {
            var error_status = $(xhr.responseText).find("p:contains(Error)").text().split(/: /);
            var error_code = parseInt(error_status[1], 10);
            var error_type = "";
            switch (error_code) {
                case 1062:
                    error_type = "Existed!";
                    break;
            }
            $("#entry-status")
                .removeClass()
                .addClass("ui-state-error").html("")
                .html("Error: Data " + error_type)
                .stop(true, true).fadeTo(0, 1).hide()
                .slideDown("normal").delay(2000).fadeOut("slow");
            $("#dlg-user-account-new").dialog("close");
        }
    });
}
</script>