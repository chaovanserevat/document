<?php

include ("../../../connection.php");
$select = "SELECT * FROM tbl_user";
$result = $connection->query($select);
	while ($user = $result->fetch_assoc()) :
		$users[] = $user;
	endwhile;

?>
<? if (COUNT($users) > 0) : ?>
<? foreach ($users as $user) : ?>
<tr>
    <td><input type="checkbox" value="<?= $user["id"] ?>" /></td>
    <td><?= $user["username"] ?></td>
    <td><?= $user["lname"] ?></td>
    <td><?= $user["fname"] ?></td>
    <td>
        <a class="link-edit" href="#<?= $user["id"]; ?>">
            <img src="img/edit.png" alt="" />
        </a>
    </td>
    <td>
        <a class="link-delete" href="#<?= $user["id"]; ?>">
            <img src="img/delete.png" alt="" />
        </a>
    </td>
</tr>
<? endforeach; ?>
<? else : ?>
<tr>
    <td colspan="4" style="color: red;">No data!</td>
</tr>
<? endif ?>