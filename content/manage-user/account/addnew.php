<?php

include ("../../../connection.php");
$select_module = $connection->query ("SELECT * FROM tbl_module");
$modules = array ();
while ($get_module = $select_module->fetch_assoc()) {
	$modules[] = $get_module;
}

?>

<div>
	<fieldset class="ui-corner-all" style="width: 300px; height: 463px; margin-right: 10px; float: left;">
		<legend>User Basic Information :</legend>
		<p>
			<label for="txt-user-last-name">Last Name</label>&nbsp;<span style="color: red;">*</span><br />
			<input id="txt-user-last-name" class="input new_theme_textbox ui-corner-all focus" type="text" maxlength="25" />
		</p>
		<p>
			<label for="txt-user-last-name">First Name</label>&nbsp;<span style="color: red;">*</span><br />
			<input id="txt-user-last-name" class="input new_theme_textbox ui-corner-all focus" type="text" maxlength="25" />
		</p>
		<p>
			<label for="txt-user-first-name">Phone</label><br />
			<input id="txt-user-first-name" class="input new_theme_textbox ui-corner-all focus" type="text" maxlength="25" />
		</p>
		<p>
			<label for="txt-user-first-name">Email</label><br />
			<input id="txt-user-first-name" class="input new_theme_textbox ui-corner-all focus" type="text" maxlength="25" />
		</p>
		<p>
			<label for="txt-user-first-name">Address</label><br />
			<textarea id="txt-user-first-name" class="input new_theme_textbox ui-corner-all focus" rows="5" cols="17"></textarea>
		</p>
		<p>
			<label for="txt-user-first-name">City</label><br />
			<input id="txt-user-first-name" class="input new_theme_textbox ui-corner-all focus" type="text" maxlength="25" />
		</p>
		<p>
			<label for="txt-user-first-name">Country</label><br />
			<input id="txt-user-first-name" class="input new_theme_textbox ui-corner-all focus" type="text" maxlength="25" />
		</p>
	</fieldset>
	
	<fieldset class="ui-corner-all">
		<legend>User Login Info :</legend>
		
		<p>
			<label for="txt-user-account-name">User Name</label>&nbsp;<span style="color: red;">*</span><br />
			<input id="txt-user-account-name" class="input new_theme_textbox ui-corner-all focus" type="text" maxlength="25" />
		</p>
		<p>
			<label for="txt-user-account-password">Password</label>&nbsp;<span style="color: red;">*</span><br />
			<input id="txt-user-account-password" class="input new_theme_textbox ui-corner-all focus" type="password" maxlength="25" />
		</p>
		<p>
			<label for="txt-user-account-confirm">Confirm</label>&nbsp;<span style="color: red;">*</span><br />
			<input id="txt-user-account-confirm" class="input new_theme_textbox ui-corner-all focus" type="password" maxlength="25" />
		</p>
	</fieldset>
	
	<fieldset class="ui-corner-all">
		<legend>User Permissions and Access :</legend>
		<? foreach ($modules AS $module) : ?>
		<p>
			<input id="<?= $module["module_id"] ?>" type="checkbox" />
			<label for="<?= $module["module_id"] ?>"><?= $module["module"] ?></label>			
		</p>
		<? endforeach; ?>
	</fieldset>
		
	<br />
	<span>Fields has asterisk(<span style="color: red;">*</span>)mark are required</span>
</div>

<script type="text/javascript">

$("#txt-user-account-name").focus().keydown(function(e) {
    if (e.keyCode == "13") {
        $(":button:contains(Save)").trigger("click");
    }
});

function save_user_account() {
    if ($("#txt-user-account-name").val() == "") {
        $("#txt-user-account-name").addClass("ui-state-error");
    } else {
        $("#txt-user-account-name").removeClass("ui-state-error");
    }
    if ($("#txt-user-account-name").hasClass("ui-state-error")) {
        $("#txt-user-account-name").focus(); return;
    }

    $.ajax({
        url: "content/manage-user/account/save.php",
        data: {
            id: 0,
			username: $("#txt-user-account-name").val(),
			password: $("#txt-user-account-password").val()
        },
        type: "post",
        dataType: "html",
        success: function(result) {
            $("#tbl-user-account tbody").load("content/manage-user/account/reload.php", function() {
                if (result == "success") {
                    $("#table-status")
                        .removeClass()
                        .addClass("ui-state-highlight")
                        .html("A user group has been saved successfully!")
                        .stop(true, true).fadeTo(0, 1).hide()
                        .slideDown("normal").delay(2000).fadeOut("slow");
                }
            });
            $("#dlg-user-account-new").dialog("close");
        },
        error: function(xhr, status, ex) {
            var error_status = $(xhr.responseText).find("p:contains(Error)").text().split(/: /);
            var error_code = parseInt(error_status[1], 10);
            var error_type = "";
            switch (error_code) {
                case 1062:
                    error_type = "Existed!";
                    break;
            }
            $("#table-status")
                .removeClass()
                .addClass("ui-state-error").html("")
                .html("Error: Data " + error_type)
                .stop(true, true).fadeTo(0, 1).hide()
                .slideDown("normal").delay(2000).fadeOut("slow");
            $("#dlg-user-account-new").dialog("close");
        }
    });
}

</script>