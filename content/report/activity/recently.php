<?php

include ("../../../connection.php");

$select_user = $connection->query ("SELECT * FROM tbl_user");
$user_option = "";

while ($get_user = $select_user->fetch_assoc()) {
	$user_option .= "<option id='{$get_user["lname"]} {$get_user["fname"]}' value='{$get_user["id"]}'>{$get_user["username"]}</option>";
}

$type = isset ($_POST["type"]) ? $_POST["type"] : "";
$selected_user = isset ($_POST["user"]) ? $_POST["user"] : "";
$selected_action = isset ($_POST["action"]) ? $_POST["action"] : "";

$where = $selected_user == "" ? "" : "WHERE tbl_user.id = '$selected_user'";
$where = $selected_user == "other" ? "WHERE tbl_user.id = '$selected_user'" : "";

if ($selected_action != "") {
	$where = $where == "" ? "WHERE tbl_activity.action = '$selected_action'" : " AND tbl_activity.action = '$selected_action'";
}	
echo $where;
$select_activity = $connection->query ("SELECT tbl_activity.*, tbl_user.username, tbl_blacklist.description AS blacklist
											FROM tbl_activity 
											LEFT JOIN tbl_user ON tbl_user.id = tbl_activity.user
											LEFT JOIN tbl_blacklist ON tbl_blacklist.user = tbl_activity.id
											$where
										");
$tbody_actities = "";
$index = 0;

while ($get_activity = $select_activity->fetch_assoc()) {
	$index++;
	$username = $get_activity["username"] == "" ? "<span style='color: red;'>anonymous</span>" : "<span style='color: blue;'>" . $get_activity["username"] . "<span>";
	$tbody_actities .= <<<ACTIVITY
						<tr>
							<td>$index</td>
							<td>$username</td>
							<td>{$get_activity["action"]}</td>
							<td>{$get_activity["date"]}</td>
							<td>{$get_activity["description"]}</td>
							<td style='color: red;'></td>
							<td style='color: red;'>{$get_activity["blacklist"]}</td>
						</tr>
ACTIVITY;
	
}

if ($type == "search") {
	echo $tbody_actities;
	
	exit ();
}

$actions = array (
	"login" => "Login",
	"delete" => "Delete",
	"insert" => "Insert",
	"update" => "Update",
	"chat" => "Chat",
	"email" => "Email",
	"blacklist" => "Blacklist"
);

$dates = array (
	"today" => "Today",
	"yesterday" => "Yesterday",
	"last-week" => "Last Week",
	"last-month" => "Last Month",
	"other" => "Other"
);

?>

<style type="text/css">

.hide-other-date {
	display: none;
}

</style>

<br />
<br />
<div id="recently-list">
	<center>
		<fieldset style="border: 1px solid #DADADA;">
			<legend>Search Activity</legend>

			<div>
				<table>
					<colgroup>
						<col style="width: 50px;" />
						<col style="width: auto;" />
					</colgroup>

					<tbody>
						<tr>
							<td><label>User</label></td>
							<td> : 
								<select id="sel-search-user">
									<option>all</option>
									<?= $user_option ?>
									<option value="other">other</option>
								</select>
								&nbsp;
								<span id="span-user-fullname"></span>
							</td>
						</tr>
						<tr>
							<td><label>Action</label></td>
							<td> : 
								<select id="sel-search-action">
									<option value="">all</option>
									<? foreach ($actions AS $key => $action) : ?>
									<option value="<?= $key ?>"><?= $action ?></option>
									<? endforeach; ?>
								</select>
							</td>
						</tr>
						<tr>
							<td><label>Date</label></td>
							<td> : 
								<select id="sel-search-date">
									<option>all</option>
									<? foreach ($dates AS $key => $date) : ?>
									<option value="<?= $key ?>"><?= $date ?></option>
									<? endforeach; ?>
								</select>
								&nbsp;
								<p id="p-other-date" class="hide-other-date" style="float: right; margin: 0px;">
									<label>from : </label> &nbsp; <input class="calendar" type="text" />&nbsp;&nbsp;
									<label>to : </label> &nbsp; <input class="calendar" type="text" />
								</p>
							</td>
						</tr>
					</tbody>
				</table>
				<p><button id="btn-show-activity">Show</button></p>
			</div>
		</fieldset>
		<br />
		<p style="font-size: 20px;">Recently Activities</p>
		<br />
		
		<table id="tbl-activity" class="tbl-data nk-tbl-data ui-corner-all" style="width: 1000px;">
			<colgroup>
				<col style="width: 15px;" />
				<col style="width: auto;" />
			</colgroup>
			
			<thead>
				<tr class="ui-widget-header">
					<th>No</th>
					<th>User</th>
					<th>Action</th>
					<th>Date</th>
					<th>Description</th>
					<th style="color: red;">Block</th>
					<th style="color: red;">Blacklist</th>
				</tr>
			</thead>
			<tbody><?= $tbody_actities ?></tbody>
		</table>
	</center>
</div>
<br />