<? session_start() ?>

<style type="text/css">

#report_list { padding: 10px; }
#report_list ul li { text-align: left; padding: 10px; }
#report_list ul li a:hover { color: red; }

#graphical { background-color: #F5F5EC; border: 1px solid #E1DBDB }
#summary { background-color: #E6EFEF; border: 1px solid #E1DBDB }
#tbl-report td { vertical-align: top; }
#detail-and-inventory { background-color: #DAEADA; border: 1px solid #E1DBDB }

#detail-and-inventory:hover, #graphical:hover, #summary:hover { background-color: #FFFFFF; border: 1px solid #FFFFFF }

a { text-decoration: none; color: blue; }

</style>
<br />
<br />
<div id="report_list">
	<center>
		<table id="tbl-report" style="width: 90%;">
			<colgroup>
				<col width="30%"></col>
				<col width="30%"></col>
				<col width="30%"></col>
			</colgroup>
			<tr>
				<td id="graphical" class="ui-corner-all" align="center">
					<div><h3 style="font-size: 20px;"><?= "Document" ?></h3>
						<ul id="ul-document">
							<li><a href="<?= "content/report/document/daily.php";?>"><?= "Daily Document" ?></a></li>
							<li><a href="<?= "reports/graphical_summary_sender";?>"><?= "Monthly Document" ?></a></li>
							<li><a href="<?= "reports/graphical_summary_ts";?>"><?= "Yearly Document" ?></a></li>
							<li><a href="<?= "reports/graphical_summary_authorize";?>"><?= "Faster Document" ?></a></li>
							<li><a href="<?= "reports/graphical_summary_taxes";?>"><?= "Created Document" ?></a></li>
						</ul>
					</div>
				</td>
				<td id="summary" class="ui-corner-all" align="center">
					<div><h3 style="font-size: 20px;"><?= "Summary & Detail" ?></h3>
						<ul>
							<li><a href="<?= "reports/summary_reciever";?>"><?= "Sender Report" ?></a></li>
							<li><a href="<?= "reports/summary_sender";?>"><?= "Authority Report" ?></a></li>
							<li><a href="<?= "reports/summary_ts";?>"><?= "Document Report" ?></a></li>
							<li><a href="<?= "reports/summary_authorize";?>"><?= "Reciever Report" ?></a></li>
						</ul>
					</div>
				</td>
				<td id="detail-and-inventory" class="ui-corner-all" align="center">
					<div><h3 style="font-size: 20px;"><?= "Activity" ?></h3>
						<ul id="ul-activity">
							<li><a href="recently"><?= "Recently Activity" ?></a></li>
							<li><a href=""><?= "Blacklist Attempt" ?></a></li>
							<li><a href=""><?= "Blocked account" ?></a></li>
							<li><a href=""><?= "Alarm Reminder" ?></a></li>
							<li><a href=""><?= "Message Box & Chat" ?></a></li>
							<li><a href=""><?= "Histroy" ?></a></li>
							<li><a href=""><?= "Forum" ?></a></li>
							<li><a href=""><?= "System Shortcut" ?></a></li>
						</ul>				
					</div>
				</td>
			</tr>
		</table>
	</center>	
</div>

<script type="text/javascript">

	$(function() {
	
		$("#ul-activity li a").click(function() {
			var href = $(this).attr("href");
			$("#content").load("content/report/activity/" + href + ".php");
			
			return false;
		});
		
		$("#select-search-user").live ("change", function() {
			var id = "real name: " + $(this).attr("id");
			$("#span-user-fullname").html(id);
		});
		
		$("#select-search-date").live ("change", function() {
			var id = $(this).val();
			$("#p-other-date").addClass("hide-other-date");
				
			if (id == "other") {
				$("#p-other-date").removeClass("hide-other-date");
			}
		});
		
		$("#btn-show-activity").live("click", function() {
			$.ajax({
				url: "<?= $_SESSION["base_url"] ?>content/report/activity/recently.php",
				type: "post",
				data: {
					type: "search",
					user: $("#sel-search-user").val(),
					action: $("#sel-search-action").val()
				},
				success: function(data) {
					$("#tbl-activity tbody").html(data);
				}
			});
		});

	});

</script>
