<?php

include ("../../../connection.php");
$select = "SELECT * FROM tbl_document_category";
$result = $connection->query($select);
$document_categories = array ();
while ($document_category = $result->fetch_assoc()) :
	$document_categories[] = $document_category;
endwhile;

$select = "SELECT * FROM tbl_standee";
$result = $connection->query($select);
$standees = array ();
while ($standee = $result->fetch_assoc()) :
	$standees[] = $standee;
endwhile;



?>

<style type="text/css">

#btn-document-upload-file {
	font-weight:normal; font-size:1em;
	font-family:Georgia,Arial, Helvetica, sans-serif;
	text-align:center;
	width:150px;
	cursor:pointer !important;
	-moz-border-radius:5px; -webkit-border-radius:5px;
}

</style>

<div>
	<br />
	<table class="formx-entry">
		<tr>
			<td style="width: 300px;">
				<p>
					<label for="txt-document-receive-in-ou">Document In/Out</label>&nbsp;<span class="star-requiredx">*</span><br />
					<select id="txt-document-receive-in-ou" class="input ui-corner-all focus requiredx">
						<option></option>
						<option value="in">In</option>
						<option value="out">Out</option>
					</select>
				</p>
				<p>
					<label for="txt-document-receive-reference-number">Reference Number</label>&nbsp;<span class="star-requiredx">*</span><br />
					<input id="txt-document-receive-reference-number" class="input new_theme_textbox ui-corner-all focus requiredx" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-origin">Document Origin</label>&nbsp;<span class="star-requiredx">*</span><br />
					<input id="txt-document-receive-origin" class="input new_theme_textbox ui-corner-all focus requiredx" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-name">Document Name</label>&nbsp;<span class="star-requiredx">*</span><br />
					<input id="txt-document-receive-name" class="input new_theme_textbox ui-corner-all focus requiredx" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-category">Document Category</label>&nbsp;<span class="star-requiredx">*</span><br />
					<select id="txt-document-receive-category" class="input new_theme_textbox ui-corner-all focus requiredx">
						<option>Select Document Category</option>
						<? foreach($document_categories as $category) : ?>
						<option value="<?= $category["id"] ?>"><?= $category["name"] ?></option>
						<? endforeach ?>
					</select>
				</p>
			</td>
			<td style="width: 300px; vertical-align: top;">
				<p>
					<label for="txt-document-receive-date-issue">Date of Document Issue</label>&nbsp;<span class="star-requiredx">*</span><br />
					<input id="txt-document-receive-date-issue" class="calendar input new_theme_textbox ui-corner-all focus requiredx" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-authority">Document Authority</label>&nbsp;<span class="star-requiredx">*</span><br />
					<input id="txt-document-receive-authority" class="input new_theme_textbox ui-corner-all focus requiredx" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-date-receive">Date of Receive Document</label>&nbsp;<span class="star-requiredx">*</span><br />
					<input id="txt-document-receive-date-receive" class="calendar input new_theme_textbox ui-corner-all focus requiredx" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-folder-number">Folder Number</label>&nbsp;<span class="star-requiredx">*</span><br />
					<input id="txt-document-receive-folder-number" class="input new_theme_textbox ui-corner-all focus requiredx" type="text" />
				</p>
			</td>
			<td style="width: 300px; vertical-align: top;">
				<p>
					<label for="txt-document-receive-standee-number">Standee Number</label>&nbsp;<span class="star-requiredx">*</span><br />
					<select id="txt-document-receive-standee-number" class="input ui-corner-all focus requiredx">
						<option></option>
						<? foreach($standees as $standee) : ?>
						<option value="<?= $standee["id"] ?>"><?= $standee["number"] ?></option>
						<? endforeach ?>
					</select>
				</p>
				<p>
					<label for="txt-document-receive-line-number">Standee Line Number</label>&nbsp;<span class="star-requiredx">*</span><br />
					<select id="txt-document-receive-line-number" class="input ui-corner-all focus requiredx">
						<option></option>
					</select>
				</p>
				<p>
					<label for="txt-document-receive-source">Source</label><br />
					<input id="txt-document-receive-source" class="input new_theme_textbox ui-corner-all focus" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-other">Other</label><br />
					<input id="txt-document-receive-other" class="input new_theme_textbox ui-corner-all focus" type="text" />
				</p>
			</td>
		</tr>
	</table>

	<p><button id="btn-document-upload-file">Upload File</button></p>

	<form id="upload-form" action="<?= base_url() ?>content/document/receive/save.php" method="post" target="uploadiframe" enctype="multipart/form-data">
		<input type="hidden" name="type" value="upload-file" />
	</form>
	<iframe id="uploadiframe" name="uploadiframe" style="width: 0px; height: 0px; border: 0 none;"></iframe>
	<p id="p-uploaded-file"></p>

	<br /><br />
	<span>Fields has asterisk(<span class="star-requiredx">*</span>)mark are required</span>
</div>

<script type="text/javascript">

	$(function() {

		$("button").button();

		/* calendar ==================================================================*/

        $(".calendar").datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true, changeYear: true, gotoCurrent: true
        });
        $(".ui-datepicker").css("font-size", "12px");
		$("#ui-datepicker-div").css("z-index", 2000);

		/* end calendar ==============================================================*/

		$("#txt-document-receive-number").focus().keydown(function(e) {
			if (e.keyCode == "13") {
				$(":button:contains(Save)").trigger("click");
			}
		});

		$("#txt-document-receive-standee-number").change(function() {
			var standee = $(this).val();
			$("#txt-document-receive-line-number").load("content/document/receive/get_standee_line.php", {
				"standee": standee
			});
		});

		$("#btn-document-upload-file").click(function() {
			$("<input id='file' name='file' type='file' />").css({
				position: "absolute",
				display: "none"
			})
			.appendTo("#upload-form")
			.trigger("click")
			.change(function() {
				$("#upload-form").submit();
			});

		});

		$(".span-cancel-upload").live("click", function() {
			var afile = $(this).attr("name");

			$.ajax ({
				url: "<?= base_url() ?>content/document/receive/save.php",
				type: "post",
				data: { type: "cancel-upload", file: $(this).attr("id") },
				success: function(result) {
					if (result == 1) $("#" + afile).remove();
					else alert("Cannot cancel this file..");
				}
			});
		});

	});

	function show_message(message) {
		$("#p-uploaded-file").append(message);
	}

	function save_new_document() {
	
		$.validatex();
	
		var uploaded_file = [];

		$(".uploaded-file").parent().find("span").each(function(i) {
			uploaded_file[i] = $(this).attr("id");
		});

		var data = [
			0,															//	0
			$("#txt-document-receive-reference-number").val(),			//	1
			$("#txt-document-receive-origin").val(),			//	2
			$("#txt-document-receive-name").val(),				//	3
			$("#txt-document-receive-category").val(),			//	4
			$("#txt-document-receive-date-issue").val(),				//	5
			$("#txt-document-receive-authority").val(),		//	6
			$("#txt-document-receive-date-receive").val(),				//	7
			$("#txt-document-receive-folder-number").val(),				//	8
			$("#txt-document-receive-standee-number").val(),			//	9
			$("#txt-document-receive-line-number").val(),				//	10
			$("#txt-document-receive-source").val(),					//	11
			$("#txt-document-receive-other").val(),						//	12
			$("#txt-document-receive-in-ou").val(),						//	13
			uploaded_file
		];

		$.ajax({
			url: "content/document/receive/save.php",
			data: {
				data: data
			},
			type: "post",
			dataType: "html",
			success: function(result) {
				$("#tbl-document-receive tbody").load("content/document/receive/reload.php", function() {
					if (result == "success") {
						$("#table-status")
							.removeClass()
							.addClass("ui-state-highlight")
							.html("A user group has been saved successfully!")
							.stop(true, true).fadeTo(0, 1).hide()
							.slideDown("normal").delay(2000).fadeOut("slow");
					}
				});
				$("#dlg-document-receive-new").dialog("close");
			},
			error: function(xhr, status, ex) {
				var error_status = $(xhr.responseText).find("p:contains(Error)").text().split(/: /);
				var error_code = parseInt(error_status[1], 10);
				var error_type = "";
				switch (error_code) {
					case 1062:
						error_type = "Existed!";
						break;
				}
				$("#table-status")
					.removeClass()
					.addClass("ui-state-error").html("")
					.html("Error: Data " + error_type)
					.stop(true, true).fadeTo(0, 1).hide()
					.slideDown("normal").delay(2000).fadeOut("slow");
				$("#dlg-document-receive-new").dialog("close");
			}
		});
	}

</script>