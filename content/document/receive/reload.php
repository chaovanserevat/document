<?php

include ("../../../connection.php");

$_in_out = isset ($_POST["in_out"]) ? $_POST["in_out"] : "";
$_where_in_out = "";

if ($_in_out != "" && $_in_out != "all")
{
	$_where_in_out = "AND in_out = '$_in_out'";
}

$select = "
			SELECT doc.*, ori.name as origin, cat.name as category, auth.name as authority, stan.number as standee, line.number as line
			FROM tbl_document doc
			INNER JOIN tbl_document_origin ori ON ori.id = doc.origin_id
			INNER JOIN tbl_document_category cat ON cat.id = doc.category_id
			INNER JOIN tbl_document_authority auth ON auth.id = doc.authority_id
			INNER JOIN tbl_line line ON line.id = doc.line_no
			INNER JOIN tbl_standee stan ON stan.id = line.standee_id
			WHERE 1 = 1 $_where_in_out
			ORDER BY date_receive_doc ASC
";

$result = $connection->query($select);

$documents = array ();
while ($document = $result->fetch_assoc()) :
	$_file_link = $connection->query ("SELECT file FROM tbl_document_file WHERE document = '{$document["id"]}'")->fetch_assoc();

	$_link = "#";
	$_target = "";

	if ($_file_link["file"] == "")
	{
		$document["title"] = "No Document File";
	}
	else
	{
		$document["title"] = "Preview Document File";
		$_link = "img/upload/document/{$_file_link["file"]}";
		$_target = "target='_blank'";
	}
	
	$_in_out_color = $document["in_out"] == "in" ? "blue" : "red";

	$document["in_out_color"] = $_in_out_color;
	$document["target"] = $_target;
	$document["file"] = $_link;
	$documents[] = $document;
endwhile;

?>
<? if (count($documents) > 0) : ?>
<? $i = 1 ?>
    <?php foreach ($documents as $document) : ?>
    <tr>
		<td><input id="<?= $document["id"] ?>" type="checkbox" /></td>
        <td style="color: <?= $document["in_out_color"] ?>;"><?= $document["reference_no"] ?></td>
        <td><?= $document["origin"] ?></td>
        <td><?= $document["name"] ?></td>
        <td><?= $document["category"] ?></td>
        <td><?= $document["date_issue_doc"] ?></td>
        <td><?= $document["authority"] ?></td>
        <td><?= $document["date_receive_doc"] ?></td>
		<td>
			<a class="link-print" href="<?= $document["file"] ?>" <?= $document["target"] ?>>
				<img src="img/view.gif" title="<?= $document["title"] ?>" />
			</a>
		</td>
		<td>
			<a class="link-edit" href="#<?= $document["id"] ?>">
				<img src="img/edit.png" title="Edit Document" />
			</a>
		</td>
		<td>
			<a class="link-delete" href="#<?= $document["id"] ?>">
				<img src="img/delete.png" title="Delete Document" />
			</a>
		</td>
    </tr>
    <?php endforeach; ?>
<? else : ?>
<tr>
    <td colspan="11" style="color: red; text-align: center;">Empty!</td>
</tr>
<? endif ?>