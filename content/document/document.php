<style type="text/css">

.link-edit img, .link-delete img {
	width: 16px;
	height: 16px;
}

</style>

<div>
	<br />
	<div style="text-align: right;">
		<div style="float: left;">Document: 
			<input id="rdo-in" name="rdo-in-out" class="rdo-in-out" type="radio" value="in" /><label for="rdo-in">in</label> / 
			<input id="rdo-out" name="rdo-in-out" class="rdo-in-out" type="radio" value="out" /><label for="rdo-out">Out</label> /
			<input id="rdo-all" name="rdo-in-out" class="rdo-in-out" type="radio" value="all" checked /><label for="rdo-all">All (In/Out)</label>
		</div>
			
		<button id="btn-document-receive-new">New Document</button>
	</div>
	<br />
	<center>
		<div style="overflow-x: scroll;">
			<p id="table-status" style="display: none; padding: 10px;"></p>

			<table id="tbl-document-receive" class="nk-tbl-data" style="width: 100%;">
				<col style="width: auto;" />
				<col style="width: 200px;" />
				<col style="width: 200px;" />
				<col style="width: 200px;" />
				<col style="width: 200px;" />
				<col style="width: 200px;" />
				<col style="width: 200px;" />
				<col style="width: 200px;" />
				<col style="width: 30px;" />
				<col style="width: 30px;" />
				<col style="width: 30px;" />
				<thead>
					<tr class="ui-widget-header">
						<th><input type="checkbox" /></th>
						<th><label id="reference_no">Reference Number</label></th>
						<th>Document Origin</th>
						<th>Document Name</th>
						<th>Category</th>
						<th>Date of Issue</th>
						<th>Document Authority</th>
						<th>Date of Receive</th>
						<th colspan="3"></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="align-center ui-state-error" colspan="12">Data not found!</td>
					</tr>	
				</tbody>
			</table>
		</div>
	</center>	
	<div id="dialog-container-1" class="dialog-container"></div>
<div>

<script type="text/javascript">

$("button").button();

$("#tbl-document-receive tbody").load("content/document/receive/reload.php", function() { $.fn.tickCheckbox() });
			
    /* create new document *********************************************/

    $("#btn-document-receive-new").click(function() {
        $("<div></div>", {
            id: "dlg-document-receive-new",
            title: "New Document"
        })
        .dialog({
            width: 890, height: 470,
            modal: true, //resizable: false,
            close: function() {
                $(this).dialog("destroy").remove();
            },
            buttons: {
                Save: function() {
                    save_new_document();
                    $(this).dialog("close");
                },
                Close: function() {
                    $(this).dialog("close");
                }
            }
        })
        .html(
			$("<center></center>").html(
				$.ajax_loading().css({ "padding-top": 5 })
			)
		)
        .load("content/document/receive/addnew.php");
    });

    /* ******************************************************************** */
		
	/* document edit ****************************************************** */
		
        $("#tbl-document-receive").delegate(".link-edit", "click", function() {
            var document_id = $(this).attr("href").split("#");
            $("<div></div>", {
                id: "dlg-document-receive-edit",
                title: "Edit Document"
            })
            .dialog({
                modal: true, //resizable: false,
				width: 890, height: 470,
                close: function() {
                    $(this).dialog("destroy").remove();
                },
                buttons: {
                    Save: function() {
                        save_edit_document();
						$(this).dialog("close");
                    },
                    Close: function() {
                        $(this).dialog("close");
                    }
                }
            })
            .html(
                $("<center></center>").html(
                    $.ajax_loading().css({ "padding-top": 5 })
                )
            )
            .load("content/document/receive/edit.php", {
                id: document_id[1]
            });
            return false;
        });
	
	/* ******************************************************************** */
	
	/* delete document **************************************************** */
		
	$("#tbl-document-receive").delegate(".link-delete", "click", function() {
		var document_id = $(this).attr("href").split("#");
		$("<div></div>", {
			id: "dlg-document-receive-delete",
			title: "Delete Document"
		})
		.html("Are you sure you want to delete this document?")
		.dialog({
			modal: true, resizable: false,
			width: 400, height: 160,
			close: function() {
				$(this).dialog("destroy").remove();
			},
			buttons: {
				Yes: function() {
					$.ajax({
						url: "content/document/receive/delete.php",
						type: "post", dataType: "html",
						data: { id: document_id[1] },
						success: function(num) {
							$("#tbl-document-receive tbody").load("content/document/receive/reload.php", function() {
								if (num > 0) {
									$("#table-status")
										.removeClass()
										.addClass("ui-state-error")
										.html("A document has been deleted successfully!")
										.stop(true, true).fadeTo(0, 1).hide()
										.slideDown("normal").delay(2000).fadeOut("slow");
								}
								
								$.fn.tickCheckbox();
							});
								
							$("#dlg-document-receive-delete").dialog("close");
						}
					});
				},
				No: function() {
					$(this).dialog("close");
				}
			}
		});
		return false;
	});
	
	$(".rdo-in-out").click(function() {
	
		$("#tbl-document-receive tbody").load("content/document/receive/reload.php", { in_out: $(this).val() }, function() { $.fn.tickCheckbox() });
	});

</script>