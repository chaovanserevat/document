<style type="text/css">

.link-edit img, .link-delete img {
	width: 16px;
	height: 16px;
}

#tbl-document-category tbody tr:nth-child(odd) { background: #EDF7F8 }
#tbl-document-category tbody tr:nth-child(even) { background: #F8F9ED }
#tbl-document-category tbody > tr:hover { background: #FFF }

</style>

<div>
	<p>
		<button id="nk-btn-new-category">New Category</button>
	</p>
	
	<div>
		<center>
			<p style="color: blue;">Document Category are showing all :</p>
			<table id="tbl-document-category" class="nk-tbl-data ui-corner-all">
				<col style="width: 10px;" />
				<col style="width: 350px;" />
				<col style="width: 580px;" />
				<col style="width: 10px;" />
				<col style="width: 10px;" />
				<thead>
					<tr class="ui-widget-header">
						<th class="align-left">#No</th>
						<th>Category Name</th>
						<th>Category Description</th>
						<th colspan="2">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="align-center ui-state-error" colspan="5">Data not found!</td>
					</tr>	
				</tbody>
			</table>
		</center>
	</div>
<div>

<script type="text/javascript">

$("button").button();

$("#tbl-document-category tbody").load("content/document-category/category/reload.php");

/* create new user account *********************************************/

$("#nk-btn-new-category").click(function() {
    $("<div></div>", {
        id: "dlg-document-category-new",
        title: "New Document Category"
    })
    .dialog({
        width: 520, height: 365,
        modal: true, resizable: false,
        close: function() {
            $(this).dialog("destroy").remove();
        },
        buttons: {
            Save: function() {
                save_document_category();
                $(this).dialog("close");
            },
            Close: function() {
                $(this).dialog("close");
            }
        }
    })
    .html(
        $("<center></center>").html(
            $.ajax_loading().css({ "padding-top": 5 })
        )
    )
    .load("content/document-category/category/addnew.php");
});

/* ********************************************************************** */

/* edit user account ******************************************************/

$("#tbl-document-category").delegate(".link-edit", "click", function() {
    var id = $(this).attr("href").split("#");
	console.log(id[1]);
    $("<div></div>", {
        id: "dlg-user-account-edit",
        title: "Edit Document Category"
    })
    .dialog({
        modal: true, resizable: false,
        width: 520, height: 440,
        close: function() {
            $(this).dialog("destroy").remove();
        },
        buttons: {
            Update: function() {
                save_document_category();
            },
            Close: function() {
                $(this).dialog("close");
            }
        }
    })

    .html(
        $("<center></center>").html(
            $.ajax_loading().css({ "padding-top": 5 })
        )
    )
    .load("content/document-category/category/edit.php", {
        id: id[1]
    });
    return false; 
	
});

/* *************************************************************************/

/* delete */

$("#tbl-document-category").delegate(".link-delete", "click", function() {
    var id = $(this).attr("href").split("#");
    $("<div></div>", {
        id: "dlg-document-category-delete",
        title: "Delete Document Category"
    })
    .html("Are you sure you want to delete this user account?")
    .dialog({
        modal: true, resizable: false,
        width: 350, height: 160,
        close: function() {
            $(this).dialog("destroy").remove();
        },
        buttons: {
            Yes: function() {
                $.ajax({
                    url: "content/document-category/category/delete.php",
                    type: "post", dataType: "html",
                    data: { id: id[1] },
                    success: function(num) {
                        $("#tbl-document-category tbody").load("content/document-category/category/reload.php", function() {
                            if (num > 0) {
                                $("#table-status")
                                    .removeClass()
                                    .addClass("ui-state-error")
                                    .html("A user account has been deleted successfully!")
                                    .stop(true, true).fadeTo(0, 1).hide()
                                    .slideDown("normal").delay(2000).fadeOut("slow");
                            }
                        });
                        $("#dlg-document-category-delete").dialog("close");
                    }
                });
            },
            No: function() {
                $(this).dialog("close");
            }
        }
    });
    return false;
});

/* *************************** */

</script>