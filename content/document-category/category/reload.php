<?php

include ("../../../connection.php");
$select = $connection->query("SELECT * FROM tbl_document_category");
	$categories = array ();
	while ($category = $select->fetch_assoc()) :
		$categories[] = $category;
	endwhile;

?>
<? if (COUNT($categories) > 0) : ?>
<? foreach ($categories as $category) : ?>
<tr>
    <td><input type="checkbox" value="<?= $category["id"] ?>" /></td>
    <td><?= $category["name"] ?></td>
    <td><?= $category["description"] ?></td>
    <td>
        <a class="link-edit" href="#<?= $category["id"]; ?>">
            <img src="img/edit.png" alt="" />
        </a>
    </td>
    <td>
        <a class="link-delete" href="#<?= $category["id"]; ?>">
            <img src="img/delete.png" alt="" />
        </a>
    </td>
</tr>
<? endforeach; ?>
<? else : ?>
<tr>
    <td colspan="4" style="color: red;">No data!</td>
</tr>
<? endif ?>