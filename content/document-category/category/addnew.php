<div>
    <p>
        <label for="txt-document-category-name">Category Name</label>&nbsp;<span style="color: red;">*</span><br />
        <input id="txt-document-category-name" class="input new_theme_textbox ui-corner-all focus" type="text" maxlength="25" />
    </p>
    <p>
        <label for="txt-document-category-description">Description</label>&nbsp;<span style="color: red;">*</span><br />
		<textarea id="txt-document-category-description" class="ui-corner-all focus" style="width: 480px; height: 120px;"></textarea>
    </p>
	<br />
	<span>Fields has asterisk(<span style="color: red;">*</span>)mark are required</span>
</div>

<script type="text/javascript">

$("#txt-document-category-name").focus().keydown(function(e) {
    if (e.keyCode == "13") {
        $(":button:contains(Save)").trigger("click");
    }
});

function save_document_category() {
    if ($("#txt-document-category-name").val() == "") {
        $("#txt-document-category-name").addClass("ui-state-error");
    } else {
        $("#txt-document-category-name").removeClass("ui-state-error");
    }
    if ($("#txt-document-category-name").hasClass("ui-state-error")) {
        $("#txt-document-category-name").focus(); return;
    }

    $.ajax({
        url: "content/document-category/category/save.php",
        data: {
            id: 0,
			name: $("#txt-document-category-name").val(),
			description: $("#txt-document-category-description").val()
        },
        type: "post",
        dataType: "html",
        success: function(result) {
            $("#tbl-document-category tbody").load("content/document-category/category/reload.php", function() {
                if (result == "success") {
                    $("#table-status")
                        .removeClass()
                        .addClass("ui-state-highlight")
                        .html("A user group has been saved successfully!")
                        .stop(true, true).fadeTo(0, 1).hide()
                        .slideDown("normal").delay(2000).fadeOut("slow");
                }
            });
            $("#dlg-document-category-new").dialog("close");
        },
        error: function(xhr, status, ex) {
            var error_status = $(xhr.responseText).find("p:contains(Error)").text().split(/: /);
            var error_code = parseInt(error_status[1], 10);
            var error_type = "";
            switch (error_code) {
                case 1062:
                    error_type = "Existed!";
                    break;
            }
            $("#table-status")
                .removeClass()
                .addClass("ui-state-error").html("")
                .html("Error: Data " + error_type)
                .stop(true, true).fadeTo(0, 1).hide()
                .slideDown("normal").delay(2000).fadeOut("slow");
            $("#dlg-document-category-new").dialog("close");
        }
    });
}

</script>