<div>
    <p>
        <label for="txt-standee-number">Standee Number</label>&nbsp;<span style="color: red;">*</span><br />
        <input id="txt-standee-number" class="input new_theme_textbox ui-corner-all focus" type="text" maxlength="25" />
    </p>
    <p>
        <label for="txt-standee-description">Description</label>&nbsp;<span style="color: red;">*</span><br />
		<textarea id="txt-standee-description" class="ui-corner-all focus" style="width: 480px; height: 120px;"></textarea>
    </p>
	<br />
	<span>Fields has asterisk(<span style="color: red;">*</span>)mark are required</span>
</div>

<script type="text/javascript">

$("#txt-standee-number").focus().keydown(function(e) {
    if (e.keyCode == "13") {
        $(":button:contains(Save)").trigger("click");
    }
});

function save_standee() {
    if ($("#txt-standee-number").val() == "") {
        $("#txt-standee-number").addClass("ui-state-error");
    } else {
        $("#txt-standee-number").removeClass("ui-state-error");
    }
    if ($("#txt-standee-number").hasClass("ui-state-error")) {
        $("#txt-standee-number").focus(); return;
    }

    $.ajax({
        url: "content/standee/standee/save.php",
        data: {
            id: 0,
			number: $("#txt-standee-number").val(),
			description: $("#txt-standee-description").val()
        },
        type: "post",
        dataType: "html",
        success: function(result) {
            $("#tbl-standee tbody").load("content/standee/standee/reload.php", function() {
                if (result == "success") {
                    $("#table-status")
                        .removeClass()
                        .addClass("ui-state-highlight")
                        .html("A user group has been saved successfully!")
                        .stop(true, true).fadeTo(0, 1).hide()
                        .slideDown("normal").delay(2000).fadeOut("slow");
                }
            });
            $("#dlg-standee-new").dialog("close");
        },
        error: function(xhr, status, ex) {
            var error_status = $(xhr.responseText).find("p:contains(Error)").text().split(/: /);
            var error_code = parseInt(error_status[1], 10);
            var error_type = "";
            switch (error_code) {
                case 1062:
                    error_type = "Existed!";
                    break;
            }
            $("#table-status")
                .removeClass()
                .addClass("ui-state-error").html("")
                .html("Error: Data " + error_type)
                .stop(true, true).fadeTo(0, 1).hide()
                .slideDown("normal").delay(2000).fadeOut("slow");
            $("#dlg-standee-new").dialog("close");
        }
    });
}

</script>