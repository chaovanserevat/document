<?php

include ("../../../connection.php");
$select = $connection->query("SELECT * FROM tbl_line");
	$lines = array ();
	while ($line = $select->fetch_assoc()) :
		$lines[] = $line;
	endwhile;

?>
<? if (COUNT($lines) > 0) : ?>
<? foreach ($lines as $line) : ?>
<tr>
    <td><input type="checkbox" value="<?= $line["id"] ?>" /></td>
    <td><?= $line["number"] ?></td>
    <td><?= $line["description"] ?></td>
    <td>
        <a class="link-edit" href="#<?= $line["id"]; ?>">
            <img src="img/edit.png" alt="" />
        </a>
    </td>
    <td>
        <a class="link-delete" href="#<?= $line["id"]; ?>">
            <img src="img/delete.png" alt="" />
        </a>
    </td>
</tr>
<? endforeach; ?>
<? else : ?>
<tr>
    <td colspan="4" style="color: red;">No Standee Line Found!</td>
</tr>
<? endif ?>