<?php

include ("../../../connection.php");
					
$select = "SELECT doc.*, ori.name as origin, cat.name as category, auth.name as authority, stan.number as standee, line.number as line
			FROM tbl_document doc
			INNER JOIN tbl_document_origin ori ON ori.id = doc.origin_id
			INNER JOIN tbl_document_category cat ON cat.id = doc.category_id
			INNER JOIN tbl_document_authority auth ON auth.id = doc.authority_id
			INNER JOIN tbl_line line ON line.id = doc.line_no
			INNER JOIN tbl_standee stan ON stan.id = line.standee_id
		";
$result = $connection->query($select);
$documents = array ();
while ($document = $result->fetch_assoc()) :
	$documents[] = $document;
endwhile;

?>
<? if (count($documents) > 0) : ?>
<? $i = 1 ?>
    <?php foreach ($documents as $document) : ?>
    <tr>
		<td><input id="<?= $document["id"] ?>" type="checkbox" /></td>
        <td><?= $document["reference_no"] ?></td>
        <td><?= $document["origin"] ?></td>
        <td><?= $document["name"] ?></td>
        <td><?= $document["category"] ?></td>
        <td><?= $document["date_issue_doc"] ?></td>
        <td><?= $document["authority"] ?></td>
        <td><?= $document["date_receive_doc"] ?></td>
		<td>
			<a class="link-edit" href="#<?= $document["id"] ?>">
				<img src="img/edit.png" alt="" />
			</a>
		</td>
		<td>
			<a class="link-delete" href="#<?= $document["id"] ?>">
				<img src="img/delete.png" alt="" />
			</a>
		</td>
    </tr>
    <?php endforeach; ?>
<? else : ?>
<tr>
    <td colspan="9" style="color: red; text-align: center;">Empty!</td>
</tr>
<? endif ?>