<?php

include ("../../../connection.php");

$where = "";

# search between date #

$to_date = isset ($_POST["to_date"]) ? $_POST["to_date"] : date("Y-m-d");

if (isset ($_POST["from_date"])) {
	$from_date = $_POST["from_date"];
	$where = "WHERE doc.date_receive_doc BETWEEN '$from_date' AND '$to_date'";
}

#######################

# search mulitple option #

$validate = "WHERE";

if (isset ($_POST["data"])) {
	$data = $_POST["data"];		
	
	foreach ($data as $info) {
		$each_info = explode("#bv#", $info);
		
		$field = $each_info[2];
		if ($field == "reference_no") {
			$table = "tbl_document";
			$field_text = "reference_no";
		}
		elseif ($field == "name") {
			$table = "tbl_document";
			$field_text = "name";
		}
		elseif ($field == "source") {
			$table = "tbl_document";
			$field_text = "source";
		}
		elseif ($field == "other") {
			$table = "tbl_document";
			$field_text = "other";
		}
		elseif ($field == "origin_id") {
			$table = "tbl_document_origin";
			$field_text = "name";
		}
		elseif ($field == "authority_id") {
			$table = "tbl_document_authority";
			$field_text = "name";
		}
		elseif ($field == "folder_no") {
			$table = "tbl_folder";
			$field_text = "number";
		}		
		
		$where .= "$validate {$each_info[0]} = '{$each_info[1]}'";
		$validate = " AND";
	}
}
##########################


$select = "SELECT doc.*, ori.name as origin, cat.name as category, auth.name as authority, stan.number as standee, line.number as line
			FROM tbl_document doc
			INNER JOIN tbl_document_origin ori ON ori.id = doc.origin_id
			INNER JOIN tbl_document_category cat ON cat.id = doc.category_id
			INNER JOIN tbl_document_authority auth ON auth.id = doc.authority_id
			INNER JOIN tbl_line line ON line.id = doc.line_no
			INNER JOIN tbl_standee stan ON stan.id = line.standee_id
			$where
		";
$result = $connection->query($select);
$documents = array ();
while ($document = $result->fetch_assoc()) :
	$documents[] = $document;
endwhile;

?>
<? if (count($documents) > 0) : ?>
<? $i = 1 ?>
    <?php foreach ($documents as $document) : ?>
    <tr>
		<td><input id="<?= $document["id"] ?>" type="checkbox" /></td>
        <td><?= $document["reference_no"] ?></td>
        <td><?= $document["origin"] ?></td>
        <td><?= $document["name"] ?></td>
        <td><?= $document["category"] ?></td>
        <td><?= $document["date_issue_doc"] ?></td>
        <td><?= $document["authority"] ?></td>
        <td><?= $document["date_receive_doc"] ?></td>
		<td>
			<a class="link-edit" href="#<?= $document["id"] ?>">
				<img src="img/edit.png" alt="" />
			</a>
		</td>
		<td>
			<a class="link-delete" href="#<?= $document["id"] ?>">
				<img src="img/delete.png" alt="" />
			</a>
		</td>
    </tr>
    <?php endforeach; ?>
<? else : ?>
<tr>
    <td colspan="9" style="color: red; text-align: center;">Empty!</td>
</tr>
<? endif ?>