<?php

	function array_2_string($sep = "-", $_data, $_prefix = "", $_suffix = "")
	{
		if ($_prefix != "" AND $_suffix != "")
		{
			$_string = array ();

			foreach ($_data AS $_value)
			{
				$_string[] = $_prefix . $_value . $_suffix;
			}
		}

		else $_string = $_data;

		return implode ("$sep", array_filter ($_string));
	}

?>