<?php

session_start();

if ($_SESSION["invalid-login"] < 4) {
	header ("location: login.php");
	exit ();
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" xml:lang="en" lang="en">
	<head>
		<link rel="shortcut icon" href="" type="image/ico">
		<title>Document Centre</title>
		<link href="css/common.css" media="screen" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<style type="text/css">
			
			* { font-family: consolas }

			#dummy {
				position: absolute;
				top: 0;
				left: 0;
				border-bottom: solid 3px #777973;
				height: 250px;
				width: 100%;
				background: url('bg-login-top.png') repeat #fff;
				z-index: 1;
			}

			#dummy2 {
				position: absolute;
				top: 0;
				left: 0;
				border-bottom: solid 2px #545551;
				height: 252px;
				width: 100%;
				background: transparent;
				z-index: 2;
			}
			
		</style>
	</head>
	<body style="background-color: black;">	
		<div class="dialog center" style="left: 50%; margin: -240px 0px 0px -460px; top: 50%; padding: 40px;">
			<div style="text-align: center; line-height: 100px; font-family: Consolas, verdana; font-size: 30px; color: white; background-color: red; width: 100%; height: 90px; position: absolute; z-index: 1000; top: 0px; left: 0px;">
				Document Managememnt Center
			</div>
			
			<div class="dialog_content">			
				<div style="margin-top: 100px;">
					<div class="content" style="height: 160px;">
						<center>
						<p style="font-size: 15px; font-family: 'Lucida Console'; color: red;">You do not have permission to access the system..</p>
						<br />
						<p style="font-size: 15px; font-family: 'Lucida Console'; color: red;">Hint! you tried to login the secure system without permission. we called you a hacker.</p>
						<p style="font-size: 15px; font-family: 'Lucida Console'; color: red;">So your account automatically were sent to blacklist.</p>
						<br />
						<p style="font-size: 15px; font-family: 'Lucida Console'; color: blue;">Please Contact your administrator :</p>
						<p style="font-size: 15px; font-family: 'Lucida Console'; color: black;"><a href="mailto:chaovaserevat@gmail">Email: chaovaserevat@gmail.com</a>, Mobile Phone: 097 81 44111</p>
						</center>
					</div>
				</div>

				<div class="dialog_buttons">
					<input type="button" id="btn-logout" class="button" style="font-size:14px" value="Live Chat">
					<input type="button" id="btn-logout" class="button" style="font-size:14px" value="Send Email">
					<input type="button" id="btn-logout" class="button" style="font-size:14px" value="Home">
				</div>
			</div>
		</div>
		
		<div id="dummy"></div>
		<div id="dummy2"></div>
		
		<script type="text/javascript">
		
			$(function() {
			
				$("#btn-logout").click(function() {
					$.ajax({
						url: "logout.php",
						type: "post",
						data: { lg: "g45dh4343423165d4f5e465341464823" },
						success: function(page) {
							window.location.href = page;
						}
					});
				});
				
				$("#btn-keep-login").click(function() {
					$.ajax({
						url: "logout.php",
						type: "post",
						data: { lg: "g45dh43434kejficxcmlghncbebcyjvnebxucyv3" },
						success: function(page) {
							window.location.href = page;
						}
					});
				});
				
			});
			
		</script>
	</body>
</html>